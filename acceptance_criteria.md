# PDF-Generator-Module - User story & acceptance criteria

**Userstory in short: The PDF-generator module should give the possibility to request user input and to display it on variable PDF forms (depending on the purpose - order, invoice...) with a company layout (header, footer) respectively variable templates. When user fills HTML form and submits them, (ONLY DOWNLOAD NOT SHOW) they should see a PDF with their data entry in new tab and be able to save the PDF easily by being asked automatically if he wants to open or save.**

## Developer customizes PDF-templates

The PDF-Generator-Module is a plugin within canal. All the customization have to be doable with blockcontroller. The developer can customize how the PDF should look like and which content they display. There should be a templating system for PDFs. This should all be customizable in code and not with a UI.

### Acceptance criteria:

#### Layout-Customization

The user should be able to customize all parameters that are customizable in a text-editing programm:
- margin-top
- martin-bottom
- font type, font size, bold, italic
- line spacing
- indentation
- images (from different sources - server, external website), with customizable size parameters

#### Handling (user) input

Controller receives php-variables that should be printable on PDF.

#### Templates

A company will have different forms. Most likely, it will want to give them the same footer, or it will have some forms that are only different in some layout features. Thus, the PDF-generator module should have some "template libraries" that can be loaded into generation function, so that layout doesn´t need to be written completely each time.

Additionally to PDF-templates, there is the possibility to create PDF from the HTML of the current page. 

#### Multiple Pages

A PDF should have the format A4. When the content is exceeding the space of one A4, it should continue on a second page. Header and footer should be identical on all pages, without having to write it every time. All pages should be saved in one PDF-file. Developer should have the possibility to customize which layout elements are displayed only once and which ones are displayed on each page.

## User gets PDF

By clicking on submit, the PDF ist generated. PDF is either generated from controller with template, or from HTML.

### Acceptance criteria:

- On server, with an predefined name (like `pdf_number.pdf`).

