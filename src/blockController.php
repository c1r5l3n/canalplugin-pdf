<?php

return static function($data) {

    if(!filter_has_var(INPUT_POST, 'submit')) {
        error_log('PDF-Form was not submitted!!');
        return '';
    } else {
        ob_start();
// Include the main TCPDF library (search for installation path).
        // require_once('tcpdf_include.php');

// create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('TCPDF');
        $pdf->SetTitle('TCPDF Example');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
        /*
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 025', PDF_HEADER_STRING);

    // set header and footer fonts
        $pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]); */

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        /*
    // set some language-dependent strings (optional)
        if(@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . dirname(__DIR__) . 'vendor');
            $pdf->setLanguageArray($l);
        } */

// ---------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

// add a page
        $pdf->AddPage();

        $pdf->SetLineWidth(2);

        $name = $_POST['input_name'];
        $checkbox = $_POST['input_checkbox'];
        $number = $_POST['input_number'];
        $date = $_POST['input_date'];

        $routing = new \Flood\Canal\Route\Routing($host, $ssl, $debug);
        $route_list = $routing->route_list;
        error_log($_SERVER['REQUEST_URI']);

        $content_file = file_get_contents(dirname(__DIR__, 4) . '/view/CurrentPageContent.html');
        $css_file = file_get_contents(dirname(__DIR__, 4) . '/data/out/style.css');

        $a = 'Anna';
//$html = '<style>' . $css_file .'</style>' . $content_file;
        $html = $content_file;


// Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

//Close and output PDF document
        ob_end_clean();
        $pdf_dir = $_SERVER['DOCUMENT_ROOT'] . 'data/out/pdf';
        $pdf->Output($pdf_dir . '/example.pdf', 'FI');
    }
};

